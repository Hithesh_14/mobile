package hybrid.automation;

import org.testng.annotations.Test;

import com.tavant.report.utils.AbstractTestCaseReport;
import com.tavant.utils.SeleniumUtils;
import com.tavant.utils.Type;

public class MobileTestingDemo extends AbstractTestCaseReport
{
	@Test
	public static void test() throws Exception
	{
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/digit_9");
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/digit_8");
		SeleniumUtils.wait(2);
	}
	
}
