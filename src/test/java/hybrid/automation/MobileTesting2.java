package hybrid.automation;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.tavant.report.utils.AbstractTestCaseReport;
import com.tavant.utils.SeleniumUtils;
import com.tavant.utils.Type;

public class MobileTesting2 extends AbstractTestCaseReport
{
	@Test
	public static void test() throws Exception
	{
		//Click on Digit '9' Buttoon
		int n = 7;
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/digit_9");
		
		//Click on Plus Button
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/op_add");
		
		int n1 = 8;
		//Click on Digit '8' button
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/digit_8");
		
		//Click on equals Button
		SeleniumUtils.click(Type.ID, "com.google.android.calculator:id/eq");
		
		//Capture the result
		String result = SeleniumUtils.getText(Type.ID, "com.google.android.calculator:id/result");
		
		SeleniumUtils.wait(2);
		
		SoftAssert asser = new SoftAssert();
			
		asser.assertEquals(result, n+n1, "Successfully verified");
		
		
		
		
	}
	
}
