package hybrid.automation;

import org.testng.annotations.Test;

import com.tavant.report.utils.AbstractTestCaseReport;
import com.tavant.utils.DriverFactory;
import com.tavant.utils.PropertyUtil;
import com.tavant.utils.SeleniumUtils;
import com.tavant.utils.Type;

public class WebTesting extends AbstractTestCaseReport
{
	@Test
	public static void test() throws Exception
	{
		SeleniumUtils.sendKeys(Type.NAME, PropertyUtil.getServerValue(SEARCH_TEXT), "abc");
		SeleniumUtils.wait(4);
	}
	
	@Test
	public static void test1() throws Exception
	{
		SeleniumUtils.sendKeys(Type.NAME, PropertyUtil.getServerValue(EMAIL_INPUT), "hitest");
		SeleniumUtils.wait(2);
	}


}
