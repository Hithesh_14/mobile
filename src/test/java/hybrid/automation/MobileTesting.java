package hybrid.automation;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.tavant.Pages.HomePage;
import com.tavant.report.utils.AbstractTestCaseReport;
import com.tavant.utils.SeleniumUtils;
import com.tavant.utils.Type;

public class MobileTesting extends AbstractTestCaseReport
{
	@Test
	public static void test1() throws Exception
	{
		//Click on Digit '9' Buttoon
		int n = 7;
		HomePage.clickOnDigit9();
		
		//Click on Plus Button
		HomePage.clickOnAddButton();
		
		int n1 = 8;
		//Click on Digit '8' button
		HomePage.ClickonDigit8();
		
		//Click on equals Button
		HomePage.clickOnEqualButton();
		
		//Capture the result
		String result = SeleniumUtils.getText(Type.ID, "com.google.android.calculator:id/result");
		
		SeleniumUtils.wait(2);
		
		SoftAssert asser = new SoftAssert();
			
		asser.assertEquals(result, n+n1, "Successfully verified");
		
			
	}
	
	
	@Test
	public static void test2() throws Exception
	{
		//Click on Digit '9' Buttoon
		int n = 7;
		HomePage.clickOnDigit9();
		
		//Click on Plus Button
		HomePage.clickOnAddButton();
		
		int n1 = 8;
		//Click on Digit '8' button
		HomePage.ClickonDigit8();
		
		//Click on equals Button
		HomePage.clickOnEqualButton();
		
		//Capture the result
		String result = SeleniumUtils.getText(Type.ID, "com.google.android.calculator:id/result");
		
		SeleniumUtils.wait(2);
		
		SoftAssert asser = new SoftAssert();
			
		asser.assertEquals(result, n+n1, "Successfully verified");
		
			
	}
	
}
