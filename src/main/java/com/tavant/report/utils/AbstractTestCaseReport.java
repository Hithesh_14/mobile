package com.tavant.report.utils;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.tavant.utils.DriverFactory;
import com.tavant.utils.PropertyUtil;

public class AbstractTestCaseReport extends DriverFactory
{
   
	static ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	static ExtentTest logger;
	static String reportDir = "";
    static String reportName = "";
    
    @BeforeMethod
    public void startUp(Method m)
   	{
    	logger = extent.createTest(m.getName());
   	}
    
	@BeforeSuite(alwaysRun = true) 
	public void startReport()
	{
		File file = new File("./Reports");
		if (!file.exists())
		{
			file.mkdir();
		}
		reportName = "TestReport_"+randomStrig() + ".html";
		String string = "./Reports/" + reportName;
		reportDir = string;
		htmlReporter = new ExtentHtmlReporter(string);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		htmlReporter.config().setDocumentTitle(PropertyUtil.getConfigValue(Project_Title));
		htmlReporter.config().setReportName(PropertyUtil.getConfigValue(Project_Title));
		htmlReporter.config().setTheme(Theme.DARK);
	}

	@AfterMethod(alwaysRun = true)
	public void getResult(ITestResult result) throws Exception
	{
		if (result.getStatus() == ITestResult.FAILURE)
		{
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
            String screenshotPath = getScreenshot(DriverFactory.getDriver(), result.getName());
            logger.addScreenCaptureFromPath(screenshotPath.replace("./Reports", ""));
		} else if (result.getStatus() == ITestResult.SKIP)
		{
			logger.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
		}
	}

	public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception
	{
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination =  "./Reports/" + screenshotName + dateName+ ".png";
		String f = screenshotName + dateName+ ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return f;
	}
	
	public static void pass(String message)
	{
		logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));
	}
	public static void info(String message)
	{
		logger.log(Status.FAIL, MarkupHelper.createLabel(message, ExtentColor.RED));
	}

	public static String randomStrig()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("ddmmSsS");
		Date date = new Date();
		return formatter.format(date);
	}

	@AfterSuite(alwaysRun = true)
	public void endReport() throws Exception
	{
		extent.flush();
	}
	
}
