package com.tavant.utils.constants;

public interface MobileObjectConstants
{
    public static final String M_HAMBURGER_SEARCH_MENU         = "M_HAMBURGER_SEARCH_MENU";

    public static final String M_HEADER_CUSTOMER_SERVICES      = "M_HEADER_CUSTOMER_SERVICES";

    public static final String M_HEADER_MAIN_NAV_TIRES         = "M_HEADER_MAIN_NAV_TIRES";

    public static final String M_FOOTER_CUSTOMER_SERVICES      = "M_FOOTER_CUSTOMER_SERVICES";

    public static final String M_FOOTER_KNOWLEDGE_CENTER       = "M_FOOTER_KNOWLEDGE_CENTER";

    public static final String M_FOOTER_FINANCING_OPTION       = "M_FOOTER_FINANCING_OPTION";

    public static final String M_FOOTER_TIRE_INSTALLER_PROGRAM = "M_FOOTER_TIRE_INSTALLER_PROGRAM";

    public static final String M_SIGN_IN                       = "M_SignIn";

}
