package com.tavant.utils.constants;


public interface FileConstants extends ObjectConstants, MobileObjectConstants
{

    public static final String TEST_HOME                 = System.getProperty("user.dir")+"/src/test/";

    public static final String RESOURCES_HOME            = TEST_HOME + "resources/";

    public static final String CONFIG_HOME               = RESOURCES_HOME + "config/";

    public static final String TESTDATA_HOME             = RESOURCES_HOME + "testdata/";

    public static final String TEST_PROPERTY_FILE        = CONFIG_HOME + "testconfig.properties";

    public static final String STATIC_TEXT_PROPERTY      = CONFIG_HOME + "staticText.properties";

    public static final String SERVER_PROPERTY_FILE      = CONFIG_HOME + "server.properties";

    public static final String OBJECT_PROPERTY_FILE      = CONFIG_HOME + "object.properties";

    public static final String MOBILE_OBJ_PROPERTY_FILE  = CONFIG_HOME + "mobileObject.properties";

    public static final String PLATFORM_NAME             = "platform.name";

    public static final String PLATFORM_VERSION          = "platform.version";

    public static final String DEVICE_NAME               = "device.name";

    public static final String BROWSE_NAME               = "browser";

    public static final String DEVICE_URL                = "driver.url";

    public static final String MOBILE_PC                 = "MOBILE_PC";

    public static final String PLATFORMEXE               = "platformexe.name";

    public static final String APP_NAME                  = "app.name";

    public static final String BROWSER_NAME              = "BROWSER_NAME";

    public static final String PROJECT_TITLE             = "Project_Title";

    public static final String EMAILS                    = "EMAILS";
    
    public static final String APP_PACKAGE               = "app.package";
    
    public static final String APP_ACTIVITY              = "app.activity";
    
    public static final String BROWSERSTACK             = "BrowerStack";

}
