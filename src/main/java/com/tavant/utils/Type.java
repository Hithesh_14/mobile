package com.tavant.utils;

public enum Type
{
    XPATH, ID, LINKTEXT, NAME, CSSSELECTOR, PARTIALLINK, CLASSNAME;
    
    
    public enum RestType
    {
    	POST, GET, PUT, DELETE, PATCH
    }
}
