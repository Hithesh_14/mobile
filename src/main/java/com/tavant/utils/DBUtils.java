package com.tavant.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBUtils 
{
	public static Connection con = null;

	public static ResultSet dBExecuteQuery(String query)
	{
		ResultSet rs = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("", "", "");
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery(query);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return rs;
	}

	public static void dBUpdateQuery(String query)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("", "", "");
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
			System.out.println("Update Sucess");
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				con.close();
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
