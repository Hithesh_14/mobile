package com.tavant.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.tavant.utils.constants.FileConstants;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory extends PropertyUtil implements FileConstants
{
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private static final String CHANGE = PropertyUtil.getConfigValue(MOBILE_PC);

    public static WebDriver getDriver()
    {
        return webDriver.get();
    }
    
    public static void setWebDriver(WebDriver driver)
    {
        webDriver.set(driver);
    }

    public static WebDriver webInstance(String browserName)
    {
        WebDriver driver = null;
        switch (browserName)
        {
            case "firefox":
            	WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case "IE":
            	WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
            case "safari":
                driver = new SafariDriver();
                break;
            case "opera":
            	WebDriverManager.operadriver().setup();
                driver = new OperaDriver();
                break;
            case "chrome":
            	WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            default:
                System.err.println(
                        "You did not mentioned the browser name exactly so defualt it is launching firefox. \nIf you mentioned the browser name please follow the rules");
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
        }
        return driver;
    }

    public static AppiumDriver<MobileElement> getmobileDriver() throws MalformedURLException
    {
    	 	String device = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("mobileDevice");
    	 	DesiredCapabilities caps = new DesiredCapabilities();
    	 	
    	 	if(getConfigValue(BROWSERSTACK).equalsIgnoreCase("YES"))
    	 	{
    	 		
    	 		caps.setCapability("os_version", "9.0");
    	 		caps.setCapability("device", "Google Pixel 3");
    	 		caps.setCapability("real_mobile", "true");
    	 		caps.setCapability("browserstack.appium_version", "1.9.1");
    	 		caps.setCapability("browserstack.local", "false"); 	 		
    	 	    caps.setCapability("project", "My First Project");
    	 	    caps.setCapability("build", "My First Build");
    	 	    caps.setCapability("name", "Bstack-[Java] Sample Test");
    	 	    caps.setCapability("app", "bs://1394ae19c5d3ecacc13275021b723b21a2a73e09");
    	 		return  new AppiumDriver<MobileElement>(new URL("https://"+getConfigValue("BS_UserName")+":"+getConfigValue("BS_AccessKey")+"@hub-cloud.browserstack.com/wd/hub"), caps);
    	 	}
    	 	
    	 	else {
    	 		
    		caps.setCapability("platformName", getConfigValue(PLATFORM_NAME));
    		//caps.setCapability("app", getConfigValue(APP_NAME));
    		caps.setCapability("platformVersion", getConfigValue(PLATFORM_VERSION));
//    		caps.setCapability("deviceName", device);
    		caps.setCapability("appPackage", getConfigValue(APP_PACKAGE));
    		caps.setCapability("appActivity", getConfigValue(APP_ACTIVITY));
    		caps.setCapability("automationName", "UiAutomator2");
    		//caps.setCapability(CapabilityType.BROWSER_NAME, getConfigValue(BROWSE_NAME));
    		//caps.setCapability(CapabilityType.PLATFORM, getConfigValue(PLATFORMEXE));
    		
    		return  new AppiumDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
    	 	}
    		
    }
    
    @BeforeMethod
    public void beforeInvocation()
    {
        try
        {
            resetWebDriver();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    

    public static void resetWebDriver() throws Exception
    {
        //quitDriver();
        if (CHANGE.equalsIgnoreCase("pc"))
        {
            System.out.println("***************************");
            WebDriver driver;
            driver = DriverFactory.webInstance(PropertyUtil.getConfigValue(BROWSER_NAME));
            DriverFactory.setWebDriver(driver);
        }
        if (CHANGE.equalsIgnoreCase("mobile"))
        {
            System.out.println("***************************");
            AppiumDriver<MobileElement> driver = DriverFactory.getmobileDriver();
            System.out.println(driver);
            DriverFactory.setWebDriver(driver);
        }
    }

    @AfterMethod
    public void afterInvocation()
    {
        quitDriver();
    }

    private static void quitDriver()
    {
        WebDriver driver = DriverFactory.getDriver();
        boolean b = driver != null;
        if (b)
        {
            driver.quit();
            driver = null;
        }
    }
}