package com.tavant.utils;

import com.tavant.utils.Type.RestType;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class RestUtils 
{

	public static Response triggerRest(RestType rest, String json, String query)
	{
		Response response = null;
		if (rest.name().equals(RestType.POST.name())) {
			response = RestAssured.given().body(json).post("");
		}
		
		if (rest.name().equals(RestType.PUT.name())) {
			response = RestAssured.given().body(json).put("");
		}
		
		if (rest.name().equals(RestType.GET.name())) {
			response = RestAssured.given().get("");
		}
		
		if (rest.name().equals(RestType.DELETE.name())) {
			response = RestAssured.given().body(json).get("");
		}
		return response;
	}
	
}
