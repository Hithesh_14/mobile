package com.tavant.utils;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.tavant.utils.constants.FileConstants;

public class SeleniumUtils extends PropertyUtil implements FileConstants
{

    public static void getURL(String url)
    {
        DriverFactory.getDriver().navigate().to(url);
    }

    public static void quitDriver()
    {
        DriverFactory.getDriver().close();
    }
    
    public static String getCSSValueLinkText(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(ele)));
        Thread.sleep(3000);
        element1.click();
        return element1.getCssValue(cssvalue);
    }

    public static String getCSSValueCssSelector(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(ele)));
        Thread.sleep(3000);
        return element1.getCssValue(cssvalue);
    }

    public static String getCSSValueXPath(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ele)));
        Thread.sleep(3000);
        return element1.getCssValue(cssvalue);
    }

    public static String getCSSValueId(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ele)));
        Thread.sleep(3000);
        return element1.getCssValue(cssvalue);
    }

    public static String getCSSValuePartialLink(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(ele)));
        Thread.sleep(3000);
        return element1.getCssValue(cssvalue);
    }

    public static void clearCookies()
    {
        DriverFactory.getDriver().manage().deleteAllCookies();
    }

    public static String getCSSValueClass(String ele, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(ele)));
        Thread.sleep(3000);
        return element1.getCssValue(cssvalue);
    }

    // click the elements
    
    public static void clickByXPath(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
        Thread.sleep(3000);
        element1.click();
    }

    
    public static void clickById(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
        Thread.sleep(3000);
        element1.click();
    }
    
    public static void clickByLinkText(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(element)));
        Thread.sleep(2000);
        element1.click();
    }
    
    public static void clickByClass(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(element)));
        Thread.sleep(3000);
        element1.click();
    }

    public static void clickByPartialLink(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(element)));
        Thread.sleep(3000);
        element1.click();
    }
    
    public static void clickByCSSSelector(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(element)));
        Thread.sleep(3000);
        element1.click();
    }
    
    public static void clickByName(String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(element)));
        Thread.sleep(3000);
        element1.click();
    }

    public static boolean PageSourceContains(String element)
    {
        return DriverFactory.getDriver().getPageSource().contains(element);
    }

    public static void sendKeysXpath(String element, String data)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
        element1.sendKeys(data);
    }

    
    public static void sendKeysPartialLink(String element, String data)
    {
        DriverFactory.getDriver().findElement(By.partialLinkText(element)).sendKeys(data);
    }
    
    public static void sendKeysClassName(String element, String data)
    {
        DriverFactory.getDriver().findElement(By.className(element)).sendKeys(data);
    }
    
    public static void sendKeysCssSelector(String element, String data)
    {
        DriverFactory.getDriver().findElement(By.cssSelector(element)).sendKeys(data);
    }
    
    public static void sendKeysLinkText(String element, String data)
    {
        DriverFactory.getDriver().findElement(By.linkText(element)).sendKeys(data);
    }
    
    public static void sendKeysById(String element, String data)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
        element1.sendKeys(data);
    }
    
    public static void sendKeysByName(String element, String data)
    {
        DriverFactory.getDriver().findElement(By.name(element)).sendKeys(data);
    }

    // onhover
    
    public static void onhoverClickLinkText(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.linkText(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.linkText(element1)).click();
    }
    
    public static void onhoverLinkText(String element)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.linkText(element));
        actions.moveToElement(ele).build().perform();
    }
    
    public static void onhoverClickCss(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.cssSelector(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.cssSelector(element1)).click();
    }
    
    public static void onhoverClickName(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.name(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.cssSelector(element1)).click();
    }
    public static void onhoverClickXpath(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.xpath(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.xpath(element1)).click();
    }
    public static void onhoverXPath(String element)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.xpath(element));
        actions.moveToElement(ele).build().perform();

    }
    public static void onhoverClickXpathLink(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.xpath(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.linkText(element1)).click();
    }
    public static void onhoverClickIdLink(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.id(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.linkText(element1)).click();
    }
    public static void onhoverClickClass(String element, String element1)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = DriverFactory.getDriver().findElement(By.className(element));
        actions.moveToElement(ele).build().perform();
        DriverFactory.getDriver().findElement(By.className(element1)).click();
    }


    // getting css values
    public static String getCssValue(Type type, String element, String cssvalue) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.getCssValue(cssvalue);
    }

    // click the elements
    public static void click(Type type, String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        Thread.sleep(1000);
        element1.click();
    }

    public static WebElement getWebElement(Type type, String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        Thread.sleep(1000);
        return element1;
    }

    // to get text
    public static String getText(Type type, String element)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.getText();
    }

    // send keys
    public static void sendKeys(Type type, String element, String data) throws InterruptedException
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        element1.clear();
        Thread.sleep(1000);
        element1.sendKeys(data);
    }

    // on hover click
    public static void onHoverAndClick(Type type, String element, Type type1, String element1) throws Exception
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebElement ele = null;
        if (type.equals(Type.CLASSNAME))
        {
            ele = DriverFactory.getDriver().findElement(By.className(element));
        } else if (type.equals(Type.CSSSELECTOR))
        {
            ele = DriverFactory.getDriver().findElement(By.cssSelector(element));
        } else if (type.equals(Type.XPATH))
        {
            ele = DriverFactory.getDriver().findElement(By.xpath(element));
        } else if (type.equals(Type.LINKTEXT))
        {
            ele = DriverFactory.getDriver().findElement(By.linkText(element));
        } else if (type.equals(Type.ID))
        {
            ele = DriverFactory.getDriver().findElement(By.id(element));
        }
        actions.moveToElement(ele).build().perform();
        SeleniumUtils.wait(1);
        if (type1.equals(Type.CLASSNAME))
        {
            DriverFactory.getDriver().findElement(By.className(element1)).click();
        } else if (type1.equals(Type.CSSSELECTOR))
        {
            DriverFactory.getDriver().findElement(By.cssSelector(element1)).click();
        } else if (type1.equals(Type.XPATH))
        {
            DriverFactory.getDriver().findElement(By.xpath(element1)).click();
        } else if (type1.equals(Type.LINKTEXT))
        {
            DriverFactory.getDriver().findElement(By.linkText(element1)).click();
        } else if (type1.equals(Type.ID))
        {
            DriverFactory.getDriver().findElement(By.id(element1)).click();
        }
    }

    // Click Using JavaScript
    public static void javaScriptClick(Type type, int offset, String element) throws Exception
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getDriver();
        int yScrollPosition = element1.getLocation().getY() - offset;
        executor.executeScript("window.scroll(0, " + yScrollPosition + ");");
        element1.click();
    }

    // Hover Using JavaScript
    public static void javaScriptOnHover(Type type, int offset, String element)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getDriver();
        int yScrollPosition = element1.getLocation().getY() - offset;
        executor.executeScript("window.scroll(0, " + yScrollPosition + ");");
        actions.moveToElement(element1).build().perform();
    }

    // On Hover
    public static void onHover(Type type, String element)
    {
        Actions actions = new Actions(DriverFactory.getDriver());
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        actions.moveToElement(element1).build().perform();
    }

    // drop downs
    public static void dropDownByVisibleText(Type type, String element, String text)
    {
        Select sel = SeleniumHelperUtils.fluentElementDropDown(type, element);
        sel.selectByVisibleText(text);
    }

    public static String getDropDownSelectedText(Type type, String element)
    {
        Select sel = SeleniumHelperUtils.fluentElementDropDown(type, element);
        String text2 = sel.getFirstSelectedOption().getText();
        return text2;
    }


    public static void dropDownByValue(Type type, String element, String value)
    {
        Select sel = SeleniumHelperUtils.fluentElementDropDown(type, element);
        sel.selectByValue(value);
    }

    public static void dropDownByIndex(Type type, String element, int index)
    {
        Select sel = SeleniumHelperUtils.fluentElementDropDown(type, element);
        sel.selectByIndex(index);
    }

    public static List<WebElement> getAllSelectedOptions(Type type, String element)
    {
        Select sel = SeleniumHelperUtils.fluentElementDropDown(type, element);
        List<WebElement> allSelectedOptions = sel.getAllSelectedOptions();
        return allSelectedOptions;
    }

    public static List<WebElement> getAllWebElementsByTagA()
    {
        return DriverFactory.getDriver().findElements(By.tagName("a"));
    }

    // get Attributes
    public static String getAttributes(Type type, String element, String attribute)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.getAttribute(attribute);
    }


    // Boolean is enabled
    public static boolean isEnabled(Type type, String element)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.isEnabled();
    }

    // Boolean is displayed
    public static boolean iSDisplayed(Type type, String element)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.isDisplayed();
    }

    public static boolean isSelected(Type type, String element)
    {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
        WebElement element1 = SeleniumHelperUtils.elements(type, element, wait);
        return element1.isSelected();
    }


    public static int wait(int time) throws Exception
    {
        int i = 1000;
        time = time * i;
        Thread.sleep(time);
        return time;
    }

    // verify from page source
    public static void PageSoourcecont(String element)
    {
        DriverFactory.getDriver().getPageSource().contains(element);
    }

    // frames switching
    public static void switchingFrameById(String frameid)
    {
        DriverFactory.getDriver().switchTo().frame(frameid);
    }

    public static void switchingFrameByNumber(int framenumber)
    {
        DriverFactory.getDriver().switchTo().frame(framenumber);
    }

    public static void switchingDefaultContent()
    {
        DriverFactory.getDriver().switchTo().defaultContent();
    }

    // Alerts
    public static void acceptAlert()
    {
        Alert alert = DriverFactory.getDriver().switchTo().alert();
        alert.accept();
    }

    public static void dismissAlert()
    {
        Alert alert = DriverFactory.getDriver().switchTo().alert();
        alert.dismiss();
    }

    public static String alertGetText()
    {
        Alert alert = DriverFactory.getDriver().switchTo().alert();
        return alert.getText();
    }

    public static void refresh()
    {
        DriverFactory.getDriver().navigate().refresh();
    }

    public static void browserBack()
    {
        DriverFactory.getDriver().navigate().back();
    }

    public static void keys(Keys keys)
    {
        Actions ac = new Actions(DriverFactory.getDriver());
        ac.sendKeys(keys);
        ac.build().perform();
    }

    public static void openNewBrowser()
    {
        if (DriverFactory.getDriver() instanceof JavascriptExecutor)
        {
            ((JavascriptExecutor) DriverFactory.getDriver()).executeScript("window.open();");
        }
    }

    public static void javaScriptExecutorUp(String position) throws Exception
    {
        JavascriptExecutor jse = (JavascriptExecutor) DriverFactory.getDriver();
        jse.executeScript("window.scrollBy(0,-" + position + ")", "");
        Thread.sleep(5000);
    }

    public static int javascriptexecutorPosition() throws Exception
    {
        JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getDriver();
        int value = (int) executor.executeScript("return window.scrollY;");
        return value;
    }

    public static void javaScriptExecutorDown(String position) throws Exception
    {
        JavascriptExecutor jse = (JavascriptExecutor) DriverFactory.getDriver();
        jse.executeScript("window.scrollBy(0," + position + ")", "");
    }

}
