package com.tavant.Pages;

import com.google.j2objc.annotations.Property;
import com.tavant.utils.PropertyUtil;
import com.tavant.utils.SeleniumUtils;
import com.tavant.utils.Type;
import com.tavant.utils.constants.ObjectConstants;

public class HomePage implements ObjectConstants {
	
	
	//Click on Digit9
	public static void clickOnDigit9() throws Exception 
	{
		System.out.println(PropertyUtil.getObjectValue(DIGIT9));	
		SeleniumUtils.click(Type.ID, PropertyUtil.getObjectValue(DIGIT9));
		
	}
	
	//Click on Add Button
	public static void clickOnAddButton() throws Exception 
	{
		
		SeleniumUtils.click(Type.ID, PropertyUtil.getObjectValue(ADDBUTTON));
		
	}
	
	//Click on Digit 8
	public static void ClickonDigit8() throws Exception 
	{
		
		SeleniumUtils.click(Type.ID, PropertyUtil.getObjectValue(DIGIT8));
		
	}
	
	public static void clickOnEqualButton() throws Exception 
	{
		
		SeleniumUtils.click(Type.ID, PropertyUtil.getObjectValue(ADDBUTTON));
		
	}
	
	


	
	
}
